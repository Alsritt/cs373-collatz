#!/usr/bin/env python3
"""
This module runs the Acceptance test for Collatz.py
"""


# -------------
# RunCollatz.py
# -------------

# -------
# imports
# -------

from sys import stdin, stdout
from Collatz import collatz_solve

# ----
# main
# ----

if __name__ == "__main__":
    collatz_solve(stdin, stdout)
