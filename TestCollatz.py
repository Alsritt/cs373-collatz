#!/usr/bin/env python3
"""
This module runs unit tests for Collatz.py using the assert framework in
Python.
"""
# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    """
    This contains the Test cases
    """
    # ----
    # read
    # ----

    def test_read(self):
        """
        Input test
        """
        string = "1 10\n"
        i, j = collatz_read(string)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Test 2
        """
        test = collatz_eval(1, 10)
        self.assertEqual(test, 20)

    def test_eval_2(self):
        """
        Test 3
        """
        test = collatz_eval(100, 200)
        self.assertEqual(test, 125)

    def test_eval_3(self):
        """
        Test 4
        """
        test = collatz_eval(201, 210)
        self.assertEqual(test, 89)

    def test_eval_4(self):
        """
        Test 5
        """
        test = collatz_eval(10, 1)
        self.assertEqual(test, 20)

    def test_eval_5(self):
        """
        Test 6
        """
        test = collatz_eval(2, 2)
        self.assertEqual(test, 2)

    def test_eval_6(self):
        """
        Test 7
        """
        test = collatz_eval(1, 100000)
        self.assertEqual(test, 351)

    def test_eval_7(self):
        """
        Test 8
        """
        test = collatz_eval(1, 1)
        self.assertEqual(test, 1)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Output test
        """
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        Complete
        """
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
