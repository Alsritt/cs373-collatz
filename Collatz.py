#!/usr/bin/env python3
"""
This program finds the most amount of equations for a single Collatz
number to reach zero inbetween a range of two numbers, inclusive.
"""

# ----------
# Collatz.py
# ----------

from typing import IO, List


CACHE_SIZE = 3000001
LAZY_CACHE = ([-1] * 3000001)

# ------------
# collatz_read
# ------------


def collatz_read(to_split: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    split_string = to_split.split()
    return [int(split_string[0]), int(split_string[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    max_cycle = 0
    if i > j:
        i, j = j, i
    if i < ((j / 2) + 1):
        i = int((j / 2) + 1)
    for val in range(i, j + 1):
        current_val = val
        cycle_length = 0
        while current_val != 1:
            if current_val < CACHE_SIZE:
                if LAZY_CACHE[current_val] != -1:
                    cycle_length += LAZY_CACHE[current_val]
                    break
            if current_val % 2 == 0:
                current_val = current_val >> 1
                cycle_length += 1
            else:
                current_val = current_val + (current_val >> 1) + 1
                cycle_length += 2
        if val < CACHE_SIZE:
            LAZY_CACHE[val] = cycle_length
        if cycle_length > max_cycle:
            max_cycle = cycle_length
    return max_cycle + 1

# -------------
# collatz_print
# -------------


def collatz_print(io_array: IO[str], start: int, end: int, max_cycle_len: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    io_array.write(str(start) + " " + str(
        end) + " " + str(max_cycle_len) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(read: IO[str], write: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for elem in read:
        first, second = collatz_read(elem)
        result = collatz_eval(first, second)
        collatz_print(write, first, second, result)
